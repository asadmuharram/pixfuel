# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
category_list = [
  ["Web Design","webdesign"],
  ["UI/UX","uiux"],
  ["Vector","vector"],
  ["Print","print"]
]
category_list.each do |category_name,category_slug|
  Category.create( category_name: category_name,category_slug: category_slug)
end

role_list = [
  "Administrator",
  "People",
]
role_list.each do |role_name|
  Role.create( role_name: role_name)
end

admin = Admin.create! :username => 'Administrator', :email => 'tes@example.com', :password => '12345678', :password_confirmation => '12345678', :role_id => '1'