class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|

      	t.string :category_name, null: false, default: ""
      	t.string :category_slug, null: false, default: ""

      	t.timestamps null: false
    end
  end
end
