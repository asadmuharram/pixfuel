class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|

    	 t.string :title, null: false, default: ""
      	t.string :url_link, null: false, default: ""
      	t.string :category_id, null: false, default: ""
      	t.text :descriptions
      	t.string :admin_id, null: false, default: ""
      	t.timestamps null: false
      	
    end
  end
end
