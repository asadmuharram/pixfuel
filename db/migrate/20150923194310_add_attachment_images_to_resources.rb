class AddAttachmentImagesToResources < ActiveRecord::Migration
  def self.up
    change_table :resources do |t|
      t.attachment :images
    end
  end

  def self.down
    remove_attachment :resources, :images
  end
end
