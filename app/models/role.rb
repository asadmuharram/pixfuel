class Role < ActiveRecord::Base
	has_many :admins

	rails_admin do

    	update do
      		field :role_name
    	end

    	list do
      		field :role_name
    	end

    	create do
      		field :role_name
    	end
  	end
end
