class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,:rememberable, :trackable, :validatable
  has_many :resources
  belongs_to :role

  def get_all_roles
   		Role.all.collect{ |u| [u.role_name, u.id] }
	end

  rails_admin do

    	update do
      		field :username
      		field :email
      		field :password
      		field :password_confirmation
      		field :role_id, :enum do
      		help 'Please select category'
        		enum_method do :get_all_roles 
       		end
      		end
    	end

    	create do
      		field :username
      		field :email
      		field :password
      		field :password_confirmation
      		field :role_id, :enum do
      		help 'Please select category'
        		enum_method do :get_all_roles 
       		end
      		end
    	end

    	list do
      		field :username
      		field :email
    	end
  	end
end
