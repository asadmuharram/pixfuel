class Category < ActiveRecord::Base
	extend FriendlyId
  	friendly_id :category_name, use: :slugged
	has_many :resources

	rails_admin do

    	update do
      		field :category_name
    	end

    	list do
      		field :category_name
    	end

    	create do
      		field :category_name
    	end
  	end
end
