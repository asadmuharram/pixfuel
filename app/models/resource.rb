class Resource < ActiveRecord::Base
	paginates_per 9
	extend FriendlyId
  	friendly_id :title, use: :slugged
	is_impressionable
	belongs_to :category
  	belongs_to :admin
 	has_attached_file :images, styles: { large: "840x540#", thumb: "440x140>" }, default_url: "/images/:style/missing.png"
  	validates_attachment_content_type :images, content_type: /\Aimage\/.*\Z/

  	def get_all_categories
   		Category.all.collect{ |u| [u.category_name, u.id] }
	end

	def self.search(search)
  		where("title LIKE ?", "%#{search}%") 
	end

  	rails_admin do

    	update do
      		field :title
      		field :url_link
      		field :descriptions
      		field :category_id, :enum do
      		help 'Please select category'
        		enum_method do :get_all_categories 
       		end
      		end
      		field :images
    	end

    	list do
      		field :title
      		field :url_link
      		field :category_name do
    			formatted_value{ bindings[:object].category.category_name }
  			end
    	end

    	create do
      		field :title
      		field :url_link
      		field :descriptions
      		field :category_id, :enum do
      		help 'Please select category'
        		enum_method do :get_all_categories 
       		end
      		end
      		field :images
    	end
  	end
end
