class HomepageController < ApplicationController

	def index
  		if params[:search]
        @resource = Resource.search(params[:search]).order("created_at DESC").page params[:page]
        @categories = Category.all.order("created_at DESC")
      else
        @resource = Resource.all.order("created_at DESC").page params[:page]
  		@categories = Category.all.order("created_at DESC")
      end
  	end

  	def categoryShow
  		@category = Category.friendly.find(params[:id])
  	end

end
